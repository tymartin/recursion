console.log('Recursion'); 

function factorial(x) {

	//Termination
	if( x < 0) return ; 

	//Base
	if (x === 0) {return 1}

		//recursion
	return x * factorial(x - 1); 


}
function checkPalindrome(s) {
	let n       = s.length 

	//termination 
	if ( n == 1 || n == 0 ) {
		console.log("It's a palindrome")
		return true; 

	} if ( s[0] == s[n-1]) {
		s = s.substr(1).slice(0,-1)
		return checkPalindrome(s)
	} else {
		console.log("its NOT a palindrome")
		return false; 
	}

}

function Queue() {
	let collection = []; 

	this.print = function() {
		console.log(collection)
	}

	this.enqueue = function(element) {
		collection.push(element)
	}
	this.dequeue = function() {
		return collection.shift(); 
	}

	this.front = function() {
		return collection[0]
	}

	this.size = function() {
		return collection.length; 
	}

	this.isEmpty = function() {
		// (collectiob.length === 0)
		if (collection.length == 0){
			return true;
		} else {
			return false; 
		}
		
	}
}


let Stack = function() {
	this.count = 0;
	this.storage = {};
	this.lowestCount = 0;
    
   this.push = function(value) {
   		this.storage[this.count] = value;
   		this.count++;
		}

	this.pop = function () {
		delete this.storage[this.count -1]; 
		this.count--;
	}

	this.peek = function () {      
		  console.log(this.storage [this.count - 1]);

	}

	this.length = function() {
		console.log(this.count)
	}

}